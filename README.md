# EXCEL TO KMZ TRANSFORMER #

![excel-kmz.jpg](https://bitbucket.org/repo/byj67a/images/677727102-excel-kmz.jpg)

Excel to KMZ Transformer is an open source (GNU GPL licence - http://www.gnu.org/licences/gpl.html) app
that reads an Excel file with geographic coordinates and draw points, polygons, linestrings, etc., in 
Google Earth.
The Excel file only need to have the rows: latitude, longitude, name and description to create an KMZ file which opens in Google Earth.

### Table of Contents ###

* Project Structure
* Links to Manual and Examples
* Screenshots
* About

### Project Structure ###

* [Distro](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/master/Distro/) - executable file made with auto-py-to-exe(https://pypi.org/project/auto-py-to-exe/)
* [setup files](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/setup%20files/?at=master) - project files to make an executable with auto-py-to-exe(https://pypi.org/project/auto-py-to-exe/)
* [src](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/?at=master) - project source code

### Links to Manual and Examples ###

* [Manual](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/manual.pdf?at=master)
* [KMZ Icons](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/icons.pdf?at=master)
* [KMZ Colors](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/colors.pdf?at=master)
* Examples:
    * KMZ with foto in the descriptive ballon:
        * [Excel](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/Foto.xlsx?at=master)
        * [KMZ](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/Foto.kmz?at=master)
    * KMZ with GPS Tracking:
        * [Excel](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/GPS.xlsx?at=master)
        * [KMZ](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/GPS.kmz?at=master)
    * KMZ with triangular Polygon:
        * [Excel](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/Celulas.xlsx?at=master)
        * [KMZ](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/Celulas.kmz?at=master)
    * KMZ with squared Polygon:
        * [Excel](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/Quadrado.xls?at=master)
        * [KMZ](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer/src/67f9ca33ca6e4ce48f5db95e2dc51d2dc8bc57e9/src/docs/Quadrado.kmz?at=master)

### Screenshot ###

* Main Window

![Excel2KMZ.PNG](https://bitbucket.org/repo/byj67a/images/2389956657-Excel2KMZ.PNG)

### About ###

* Developed at Boris & Vladimir Software, a fictious team working for the DP - UAF - GNR, by Nuno Ven�ncio